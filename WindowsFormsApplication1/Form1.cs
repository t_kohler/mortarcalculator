﻿using System;
using System.Windows.Forms;

namespace DistanceCalculator
{
    public partial class Form1 : Form
    {
        private int historyLength = 5;

        public Form1()
        {
            InitializeComponent();
        }


        private void textBox_Click(object sender, EventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        private void textBox_Focus(object sender, EventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            if(textBox.TextLength == 5)
            {
                SelectNextControl(textBox, true, true, true, true);
            }
        }

        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            var from = this.textBox1.Text;
            var to = this.textBox3.Text;

            var l = Int32.Parse(textBox1.Text);
            var m = Int32.Parse(textBox2.Text);
            var n = Int32.Parse(textBox3.Text);
            var o = Int32.Parse(textBox4.Text);

            var angle = 90-(Math.Atan2(o - m, n - l) * (180.0 / Math.PI));

            if (angle < 0)
            {
                angle += 360;
            }

            var mil = (6400d / 360d) * angle;

            textBox6.Text = String.Format("{0:0.0}", angle);
            textBox7.Text = String.Format("{0:0}", mil);

            var x = Math.Abs(l - n);
            var y = Math.Abs(m - o);

            var p = Math.Sqrt(Math.Pow(x,2) + Math.Pow(y,2));
            textBox5.Text = String.Format("{0:0}", p);

            listBox1.Items.Insert(0, String.Format("{0:00000} {1:00000} - {2:00000} {3:00000} : {4:0} m @ {5:0.0}° / {6:0} mil", l, m, n, o, p, angle, mil));
            if (listBox1.Items.Count > historyLength)
            {
                listBox1.Items.RemoveAt(listBox1.Items.Count - 1);
            }
            listBox1.SelectedIndex = 0;
            listBox1.ClearSelected();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textBox5.Text);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MortarCalculator
{
    public partial class InputWindow : Form
    {
        private readonly Calculator _calculator;
        private readonly Func<string, bool> _func;
        private static InputWindow inputWindow;

        private InputWindow(string formAction, Calculator calculator, Func<string, bool> func)
        {
            this._calculator = calculator;
            this._func = func;
            _calculator.Enabled = false;
            InitializeComponent();
            Text = formAction;
        }

        private void saveClicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.textBox1.Text))
            {
                if (_func(this.textBox1.Text))
                {
                    close();
                }
            }
            else
            {
                MessageBox.Show("Invalid name", "Nameing error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void cancelClicked(object sender, EventArgs e)
        {
            close();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            close();
            base.OnFormClosing(e);
        }

        public static void openInputWindow(string formAction, Calculator calculator, Func<string, bool> func)
        {
            inputWindow = new InputWindow(formAction, calculator, func);
            inputWindow.Show();
        }

        private void close()
        {
            _calculator.Enabled = true;
            inputWindow.Dispose();
            inputWindow.Close();
        }
    }
}

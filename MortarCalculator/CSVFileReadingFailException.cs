﻿using System;

public class CSVFileReadingFailException : Exception
{
    public CSVFileReadingFailException()
    {
    }

    public CSVFileReadingFailException(string message)
        : base(message)
    {
    }

    public CSVFileReadingFailException(string message, Exception inner)
        : base(message, inner)
    {
    }
}
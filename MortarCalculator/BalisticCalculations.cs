﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MortarCalculator
{
    class BalisticCalculations
    {
        private static string _activeMortar;
        private static int _activeCharge;

        private static readonly Dictionary<string, Dictionary<int, string>> files = new Dictionary<string, Dictionary<int, string>>()
        {
            {"mk6", new Dictionary<int, string>() {
                {0, @".\rangetables\mk6_charge0.csv" },
                {1, @".\rangetables\mk6_charge1.csv" },
                {2, @".\rangetables\mk6_charge2.csv" }
            } },
            {"mk6 adv", new Dictionary<int, string>() {
                {0, @".\rangetables\mk6adv_charge0.csv" },
                {1, @".\rangetables\mk6adv_charge1.csv" },
                {2, @".\rangetables\mk6adv_charge2.csv" }
            } },
            {"m6", new Dictionary<int, string>() {
                {0, @".\rangetables\m6_charge0.csv" },
                {1, @".\rangetables\m6_charge1.csv" },
                {2, @".\rangetables\m6_charge2.csv" },
                {3, @".\rangetables\m6_charge3.csv" }
            } },
            {"2b14 adv", new Dictionary<int, string>() {
                {0, @".\rangetables\2b14adv_charge0.csv" },
                {1, @".\rangetables\2b14adv_charge1.csv" },
                {2, @".\rangetables\2b14adv_charge2.csv" }
            } }
        };

        private static readonly double DEFAULT_TEMP = 15.0; //°C
        //private static readonly double DEFAULT_PRESSURE = 1013.25; //hPa
        private static readonly double DEFAULT_DENSITY = 1.225; //kg / m^3
        private static readonly double ABSOLUTE_ZERO_IN_CELSIUS = -273.15;
        private static readonly double UNIVERSAL_GAS_CONSTANT = 8.314;
        private static readonly double WATER_VAPOR_MOLAR_MASS = 0.018016;
        private static readonly double DRY_AIR_MOLAR_MASS = 0.028964;
        private static readonly double SPECIFIC_GAS_CONSTANT_DRY_AIR = 287.058;
        private static Dictionary<int, Dictionary<int, Dictionary<string, double>>> _rangeTable;

        public static double[] Calculate(string mortar, int charge, double humidity, double pressure, double temp, int distance, double gunAlt, double tarAlt, double crWind, double heTaWind)
        {
            var list = new List<Dictionary<int, Dictionary<string, double>>>();
            try
            {
                if (!mortar.Equals(_activeMortar))
                {
                    list.AddRange(files[mortar].Select(entry => RangeTableCSVFileReader.read_file(entry.Value)));

                    _rangeTable = new Dictionary<int, Dictionary<int, Dictionary<string, double>>>();
                    for (var i = 0; i < list.Count; i++)
                    {
                        _rangeTable.Add(i, list[i]);
                    }
                    _activeMortar = mortar;
                }
                _activeCharge = charge;

                return compute_elevation(humidity, pressure, temp, distance, gunAlt, tarAlt, crWind, heTaWind);
            }
            finally {
                if (list.Count < files[_activeMortar].Count)
                {
                    list = null;
                }
            }
        }

        private static double[] compute_elevation(double humidity, double pressure, double temp, int distance, double gunAlt, double tarAlt, double crWind, double heTaWind)
        {
            // computes tof, elevation and lateral adjustement based on parameters
            var diffAlt = gunAlt - tarAlt;
            var diffTemp = temp - DEFAULT_TEMP;
            var diffDensity = calcAirDensity(temp, pressure, humidity) - DEFAULT_DENSITY;
            if (!_rangeTable[_activeCharge].ContainsKey(distance) && !linear_approx(distance)) {
                return null;
            }

            var tableEntry = _rangeTable[_activeCharge][distance];
            double range = distance;

            if (diffTemp < 0)
            {
                range += diffTemp * tableEntry["air_temp_dec"];
            }
            else
            {
                range += diffTemp * tableEntry["air_temp_inc"] * -1;
            }
            if (diffDensity < 0) {
                range += (diffDensity / DEFAULT_DENSITY) * 100 * tableEntry["air_dens_dec"] * -1;
            }
            else {
                range += (diffDensity / DEFAULT_DENSITY) * 100 * tableEntry["air_dens_inc"] * -1;
            }
            if (heTaWind < 0) // headwind positive, tailwind negative, same in kestrel
            {
                range += heTaWind * tableEntry["d_tail_wind"];
            }
            else
            {
                range += heTaWind * tableEntry["d_head_wind"] * -1;
            }

            distance = (int)Math.Round(range);

            if (!_rangeTable[_activeCharge].ContainsKey(distance) && !linear_approx(distance))
            {
                return null;
            }
            tableEntry = _rangeTable[_activeCharge][distance];

            var elevation = tableEntry["elev"];
            elevation += diffAlt * tableEntry["d_elev"] / 100;

            var azimuth = crWind * tableEntry["d_cr_wind"];
            var tof = tableEntry["tof"] + diffAlt / 100.0 * tableEntry["d_tof"];
            return new [] { elevation, azimuth, tof };
        }

        private static bool linear_approx(int distance) { //approximates points in between the ranges in the range table linearly, adds the new range to the dict
            var distanceStep = 50;
            if ((_activeMortar == "m6") && (_activeCharge < 2))
            {
                distanceStep = 25;
            }
            var lower = distance / distanceStep * distanceStep;
            var higher = lower + distanceStep;
            var factor = (distance - lower) / (double)distanceStep;
            if (!_rangeTable[_activeCharge].ContainsKey(higher) || !_rangeTable[_activeCharge].ContainsKey(lower)) {
                //TODO error message
                return false;
            }
            _rangeTable[_activeCharge][distance] = new Dictionary<string, double>();
            foreach (KeyValuePair<string, double> entry in _rangeTable[_activeCharge][lower])
            {
                _rangeTable[_activeCharge][distance].Add(entry.Key, entry.Value + (_rangeTable[_activeCharge][higher][entry.Key] - entry.Value) * factor);
            }
            return true;
        }

        private static double calcAirDensity(double temperature, double pressure, double humidity)
        {
            pressure *= 100; // hPa to Pa
            humidity /= 100; //% to decimal
            double density;

            if (humidity > 0)   // Saturation vapor pressure calculated according to: http://wahiduddin.net/calc/density_algorithms.htm
            {
                var pSat = 6.1078 * Math.Pow(10,((7.5 * temperature) / (temperature + 237.3)));
                //var pSat = 610.78 * Math.Pow(10,((7.5 * temperature) / (temperature + 237.3))); TODO: change to this after ACE3 update
                var vaporPressure = humidity * pSat;
                var partialPressure = pressure - vaporPressure;

                density = (partialPressure * DRY_AIR_MOLAR_MASS + vaporPressure * WATER_VAPOR_MOLAR_MASS) / (UNIVERSAL_GAS_CONSTANT * kelvin(temperature));
            } else {
                density = pressure / (SPECIFIC_GAS_CONSTANT_DRY_AIR * kelvin(temperature));
            };

            return density;
        }

        private static double kelvin(double t) {
            return (t - ABSOLUTE_ZERO_IN_CELSIUS);
        }
    }
}
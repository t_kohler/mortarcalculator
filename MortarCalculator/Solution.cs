﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MortarCalculator
{
    public class Solution
    {
        public string Name { get; set; }

        public string Mortar { get; }

        public int Charge { get; }

        public double GunAlt { get; }

        public double TarAlt { get; }

        public int Distance { get; }

        public int DirectionMil { get; }

        public double DirectionDeg { get; }

        public int GunCoordTop { get; }

        public int GunCoordLeft { get; }

        public int TarCoordTop { get; }
        
        public int TarCoordLeft { get; }

        public int ElevMil { get; }

        public double ElevDeg { get; }

        public int AzimCorMil { get; }

        public double AzimCorDeg { get; }

        public int AzimMil { get; }

        public double AzimDeg { get; }

        public double Tof { get; }

        public Solution(
            string name,
            string mortar,
            int charge,
            double gunAlt,
            double tarAlt,
            int distance,
            int direction,
            int gunCoordTop,
            int gunCoordLeft,
            int tarCoordTop,
            int tarCoordLeft,
            double elev,
            double azimCor,
            double azim,
            double tof
            )
        {
            this.Name = name;
            this.Mortar = mortar;
            this.Charge = charge;
            this.GunAlt = gunAlt;
            this.TarAlt = tarAlt;
            this.Distance = distance;
            this.DirectionMil = direction;
            this.DirectionDeg = direction / 6400d * 360d;
            this.GunCoordTop = gunCoordTop;
            this.GunCoordLeft = gunCoordLeft;
            this.TarCoordTop = tarCoordTop;
            this.TarCoordLeft = tarCoordLeft;
            this.ElevMil = (int)Math.Round(elev);
            this.ElevDeg = elev / 6400 * 360;
            this.AzimCorMil = (int)Math.Round(azimCor);
            this.AzimCorDeg = azimCor / 6400d * 360d;
            this.AzimMil = (int)Math.Round(azim);
            this.AzimDeg = azim / 6400d * 360d;
            this.Tof = tof;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}

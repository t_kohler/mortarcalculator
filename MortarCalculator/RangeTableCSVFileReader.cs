﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MortarCalculator
{
    public class RangeTableCSVFileReader
    {
        /// <exception cref="CSVFileReadingFailException">File could not be read.</exception>
        public static Dictionary<int, Dictionary<string, double>> read_file(string path)
        {
            var rangeTableSubPage = new Dictionary<int, Dictionary<string, double>>();
            FileStream fs = null;
            try
            {
                fs = File.OpenRead(path);
                StreamReader reader = null;
                try
                {
                    reader = new StreamReader(fs);

                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(',');
                        rangeTableSubPage.Add(int.Parse(values[0]), new Dictionary<string, double>() {
                            {"elev", double.Parse(values[1], System.Globalization.CultureInfo.InvariantCulture)},
                            {"d_elev", double.Parse(values[2], System.Globalization.CultureInfo.InvariantCulture)},
                            {"d_tof", double.Parse(values[3], System.Globalization.CultureInfo.InvariantCulture)},
                            {"tof", double.Parse(values[4], System.Globalization.CultureInfo.InvariantCulture)},
                            {"d_cr_wind", double.Parse(values[5], System.Globalization.CultureInfo.InvariantCulture)},
                            {"d_head_wind", double.Parse(values[6], System.Globalization.CultureInfo.InvariantCulture)},
                            {"d_tail_wind", double.Parse(values[7], System.Globalization.CultureInfo.InvariantCulture)},
                            {"air_temp_dec", double.Parse(values[8], System.Globalization.CultureInfo.InvariantCulture)},
                            {"air_temp_inc", double.Parse(values[9], System.Globalization.CultureInfo.InvariantCulture)},
                            {"air_dens_dec", double.Parse(values[10], System.Globalization.CultureInfo.InvariantCulture)},
                            {"air_dens_inc", double.Parse(values[11], System.Globalization.CultureInfo.InvariantCulture)},
                        });

                    }
                }
                finally
                {
                    if (reader != null)
                        ((IDisposable)reader).Dispose();
                }
            }
            catch (ArgumentException e) { fileErrorHandler(e, "Argument Exception"); }
            catch (PathTooLongException e) { fileErrorHandler(e, "Path Too Long"); }
            catch (DirectoryNotFoundException e) { fileErrorHandler(e, "Directory Not Found"); }
            catch (UnauthorizedAccessException e) { fileErrorHandler(e, "Unauthorized Access"); }
            catch (FileNotFoundException e) { fileErrorHandler(e, "File Not Found"); }
            catch (NotSupportedException e) { fileErrorHandler(e, "NotSupportedException <- ??"); }
            catch (IOException e) { fileErrorHandler(e, "IOException: shit hit the fan"); }
            finally
            {
                if (fs != null)
                {
                    ((IDisposable)fs).Dispose();
                }
            }
            return rangeTableSubPage;
        }



        private static void fileErrorHandler(Exception e, string message)
        {
            // TODO: write to a log, whatever...
            throw new CSVFileReadingFailException(message, e);
        }
    }
}
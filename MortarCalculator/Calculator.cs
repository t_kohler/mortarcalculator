﻿using System;
using System.Windows.Forms;

namespace MortarCalculator
{
    public partial class Calculator : Form
    {
        private Solution _currentSolution;

        public Calculator()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
        }

        private void textBox_Click(object sender, EventArgs e)
        {
            ((TextBox) sender).SelectAll();
        }

        private void textBox_Focus(object sender, EventArgs e)
        {
            ((TextBox) sender).SelectAll();
        }

        private void textBoxCoordinates_TextChanged(object sender, EventArgs e)
        {
            var textBox = (TextBox) sender;
            if (textBox.TextLength == 5)
            {
                SelectNextControl(textBox, true, true, true, true);
            }
        }

        private void textBoxCoordinates_FocusLost(object sender, EventArgs e)
        {
            var textBox = (TextBox) sender;
            if (textBox.Text != "")
            {
                while (textBox.TextLength < 5)
                {
                    textBox.Text += "0";
                }
            }
        }

        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void textBox_KeyPress_DetectDecimal(object sender, KeyPressEventArgs e)
        {
            var textBox = (TextBox) sender;
            if (e.KeyChar.Equals('.'))
            {
                if (textBox.Text.Contains("."))
                {
                    e.Handled = true;
                }
                else if (textBox.Text.Length == 0)
                {
                    textBox.Text = "0.";
                    textBox.SelectionStart = 2;
                    e.Handled = true;
                }
            }
            else if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void textBox_KeyPress_DetectDecimalAndNegatives(object sender, KeyPressEventArgs e)
        {
            var textBox = (TextBox) sender;
            if (e.KeyChar.Equals('.'))
            {
                if (textBox.Text.Contains("."))
                {
                    e.Handled = true;
                }
                else if (textBox.Text.Length == 0)
                {
                    textBox.Text = "0.";
                    textBox.SelectionStart = 2;
                    e.Handled = true;
                }
                else if (textBox.Text.Length == 1 && textBox.Text == "-")
                {
                    textBox.Text = "-0.";
                    textBox.SelectionStart = 3;
                    e.Handled = true;
                }
            }
            else if (e.KeyChar.Equals('-'))
            {
                if (textBox.Text.Length > 0)
                {
                    e.Handled = true;
                }
            }
            else if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var l = int.Parse(this.textBox9.Text);
                var m = int.Parse(this.textBox10.Text);
                var n = int.Parse(this.textBox11.Text);
                var o = int.Parse(this.textBox12.Text);

                var angle = 90 - (Math.Atan2(o - m, n - l) * (180.0 / Math.PI));

                if (angle < 0)
                {
                    angle += 360;
                }

                var mil = (6400d / 360d) * angle;

                //textBox6.Text = String.Format("{0:0.0}", angle);
                this.textBox16.Text = $@"{mil:0}";

                var x = Math.Abs(l - n);
                var y = Math.Abs(m - o);

                var p = Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));
                this.textBox15.Text = $@"{p:0}";
            }
            catch (FormatException)
            {
                return;
                //TODO: handle
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                var mortar = this.comboBox1.GetItemText(this.comboBox1.SelectedItem);
                var charge = this.comboBox2.SelectedIndex;
                var distance = int.Parse(this.textBox15.Text);
                var gunAlt = double.Parse(this.textBox13.Text, System.Globalization.CultureInfo.InvariantCulture);
                var tarAlt = double.Parse(this.textBox14.Text, System.Globalization.CultureInfo.InvariantCulture);


                var humidity = 0d;
                var pressure = 0d;
                var temp = 0d;
                var heTaWind = 0d;
                var crWind = 0d;

                if (mortar.Contains(" adv"))
                {

                    humidity = double.Parse(this.textBox2.Text, System.Globalization.CultureInfo.InvariantCulture);
                    pressure = double.Parse(this.textBox3.Text, System.Globalization.CultureInfo.InvariantCulture);
                    temp = double.Parse(this.textBox1.Text, System.Globalization.CultureInfo.InvariantCulture);
                    heTaWind = double.Parse(this.textBox5.Text, System.Globalization.CultureInfo.InvariantCulture);
                    crWind = double.Parse(this.textBox7.Text, System.Globalization.CultureInfo.InvariantCulture);
                }

                var tmp = BalisticCalculations.Calculate(mortar, charge, humidity, pressure, temp, distance, gunAlt,
                    tarAlt, crWind, heTaWind);

                if (tmp == null) throw new OutOfRangeException();
                var elev = tmp[0];
                var azimCor = tmp[1];
                double azim =-1;
                var tof = tmp[2];

                this.textBox17.Text = $@"{Math.Round(elev):0}";
                this.textBox18.Text = $@"{elev / 6400 * 360:0.0}";
                this.textBox19.Text = $@"{Math.Round(azimCor):0}";
                this.textBox20.Text = $@"{azimCor / 6400 * 360:0.0}";
                this.textBox23.Text = $@"{tof:0.0}";

                var dir = this.textBox16.Text;
                this.textBox21.Text = !string.IsNullOrEmpty(dir)
                    ? $@"{Math.Round((int.Parse(dir) + ((tmp[1] / 360) * 6400)), 1):0}"
                    : "";
                if (!string.IsNullOrEmpty(dir))
                {
                    azim = int.Parse(dir) + azimCor;
                    this.textBox21.Text = $@"{Math.Round(azim):0}";
                    this.textBox22.Text = $@"{azim / 6400d * 360:0}";
                }
                else
                {
                    this.textBox21.Text = "";
                    this.textBox22.Text = "";
                }

                int direction = -1;
                if (!string.IsNullOrEmpty(this.textBox16.Text))
                {
                    direction = int.Parse(this.textBox16.Text);
                }

                var gunCoordTop = string.IsNullOrEmpty(this.textBox9.Text) ? -1 : int.Parse(this.textBox9.Text);
                var gunCoordLeft = string.IsNullOrEmpty(this.textBox10.Text) ? -1 : int.Parse(this.textBox10.Text);
                var tarCoordTop = string.IsNullOrEmpty(this.textBox11.Text) ? -1 : int.Parse(this.textBox11.Text);
                var tarCoordLeft = string.IsNullOrEmpty(this.textBox12.Text) ? -1 : int.Parse(this.textBox12.Text);

                _currentSolution = new Solution("current", mortar, charge, gunAlt, tarAlt, distance, direction, gunCoordTop,
                    gunCoordLeft, tarCoordTop, tarCoordLeft, elev, azimCor, azim, tof);
            }
            catch (CSVFileReadingFailException ex)
            {
                handleErrorAlert("The range table CSV file could not be read: \n" + ex.Message, "File Read Error",
                    MessageBoxIcon.Error, ex);
            }
            catch (OutOfRangeException ex)
            {
                handleErrorAlert("No solution found for selected charge and parameters \ntry different charge",
                    "No solution found", MessageBoxIcon.Information, ex);
            }
            catch (FormatException ex)
            {
                handleErrorAlert("Not all fields necessary for calculation filled are out", "Form incomplete",
                    MessageBoxIcon.Warning, ex);
            }
        }

        private void charge_Change(object sender, EventArgs e)
        {
            if ((string) this.comboBox1.SelectedItem == "m6")
            {
                this.comboBox2.Items.Add("Charge 3");
            }
            else if (this.comboBox2.Items.Contains("Charge 3"))
            {
                this.comboBox2.Items.Remove("Charge 3");
            }
            if (this.comboBox1.GetItemText(this.comboBox1.SelectedItem).Contains(" adv"))
            {
                this.textBox2.ReadOnly = false;
                this.textBox2.TabStop = true;
                this.textBox3.ReadOnly = false;
                this.textBox3.TabStop = true;
                this.textBox1.ReadOnly = false;
                this.textBox1.TabStop = true;
                this.textBox5.ReadOnly = false;
                this.textBox5.TabStop = true;
                this.textBox7.ReadOnly = false;
                this.textBox7.TabStop = true;
            }
            else
            {
                this.textBox2.ReadOnly = true;
                this.textBox2.TabStop = false;
                this.textBox3.ReadOnly = true;
                this.textBox3.TabStop = false;
                this.textBox1.ReadOnly = true;
                this.textBox1.TabStop = false;
                this.textBox5.ReadOnly = true;
                this.textBox5.TabStop = false;
                this.textBox7.ReadOnly = true;
                this.textBox7.TabStop = false;
            }
                
        }

        private void clearAll_Click(object sender, EventArgs e)
        {
            this.textBox1.Text = "";
            this.textBox3.Text = "";
            this.textBox5.Text = "";
            this.textBox7.Text = "";
            this.textBox9.Text = "";
            this.textBox10.Text = "";
            this.textBox11.Text = "";
            this.textBox12.Text = "";
            this.textBox13.Text = "";
            this.textBox14.Text = "";
            this.textBox15.Text = "";
            this.textBox16.Text = "";
            this.textBox17.Text = "";
            this.textBox18.Text = "";
            this.textBox19.Text = "";
            this.textBox20.Text = "";
            this.textBox21.Text = "";
            this.textBox22.Text = "";
            this.textBox23.Text = "";
            _currentSolution = null;
        }

        private void saveSolutionAs_Click(object sender, EventArgs e)
        {
            if (_currentSolution != null)
            {
                InputWindow.openInputWindow("Save solution", this, saveSlution);
            }
            else
            {
                //TODO: replace alert with functionality
                MessageBox.Show("A solution has to be calculated first in order to save it", "No solution found", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        private void renameSelectedSavedSolution_Click(object sender, EventArgs e)
        {
            if (this.listBox1.SelectedItem != null)
            {
                InputWindow.openInputWindow("Rename solution", this, renameSavedSlution);
            }
            else
            {
                //TODO: replace alert with functionality
                MessageBox.Show("A solution has to be selected from the list first in order to rename it", "No solution selected", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        private void deleteSelectedSavedSolution_Click(object sender, EventArgs e)
        {
            if (this.listBox1.SelectedItem != null)
            {
                this.listBox1.Items.Remove(this.listBox1.SelectedItem);
            }
            else
            {
                //TODO: replace alert with functionality
                MessageBox.Show("A solution has to be selected from the list first in order to delete it", "No solution selected", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        private static void handleErrorAlert(string message, string title, MessageBoxIcon msgIcon, Exception ex)
        {
            MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            Console.WriteLine("####");
            Console.WriteLine("####");
            Console.Write(ex.ToString());
            Console.WriteLine();
            Console.WriteLine("####");
            Console.WriteLine("####");
        }

        private bool renameSavedSlution(string name)
        {
            if (nameAlreadyTaken(name))
            {
                MessageBox.Show("There already is a solution with that name in the list", "Name already taken", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            var i = this.listBox1.SelectedIndex;
            var sol = (Solution) this.listBox1.Items[i];
            sol.Name = name;
            listBox1.Items[i] = sol;
            return true;
        }

        private bool saveSlution(string name)
        {
            if (nameAlreadyTaken(name))
            {
                MessageBox.Show("There already is a solution with that name in the list", "Name already taken", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            _currentSolution.Name = name;
            var i = this.listBox1.Items.Add(_currentSolution);
            //listBox1.re
            return true;
        }

        private bool nameAlreadyTaken(string name)
        {
            foreach (var listBox1Item in listBox1.Items)
            {
                if (listBox1Item.ToString() == name)
                {
                    return true;
                }
            }
            return false;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
            {
                this.textBox4.Text ="";
                this.textBox6.Text = "";
                this.textBox8.Text = "";
                this.textBox24.Text = "";
                this.textBox32.Text = "";
                this.textBox33.Text = "";
                this.textBox34.Text = "";
                this.textBox35.Text = "";
                this.textBox27.Text = "";
                this.textBox28.Text = "";
                this.textBox29.Text = "";
                this.textBox30.Text = "";
                this.textBox25.Text = "";
                this.textBox26.Text = "";
                this.textBox31.Text = "";
                return;
            }
            var sol = (Solution) listBox1.SelectedItem;

            var coordinatesAreSet = true;
            if (sol.GunCoordTop < 0 || sol.GunCoordLeft < 0 || sol.TarCoordTop < 0 || sol.TarCoordTop < 0)
            {
                this.textBox4.Text = "";
                this.textBox6.Text = "";
                this.textBox8.Text = "";
                this.textBox24.Text = "";
            }
            else
            {
                this.textBox4.Text = $@"{sol.GunCoordTop:00000}";
                this.textBox6.Text = $@"{sol.GunCoordLeft:00000}";
                this.textBox8.Text = $@"{sol.TarCoordTop:00000}";
                this.textBox24.Text = $@"{sol.TarCoordTop:00000}";
            }

            this.textBox32.Text = $@"{sol.GunAlt:0}";
            this.textBox33.Text = $@"{sol.TarAlt:0}";
            this.textBox34.Text = (sol.Distance < 0) ? "" : $@"{sol.Distance:0}";
            if (sol.DirectionMil < 0)
            {
                this.textBox35.Text = "";
                this.textBox35.Text = "";
            }
            else
            {
                this.textBox35.Text = $@"{sol.DirectionMil:0}";
                this.textBox35.Text = $@"{sol.DirectionDeg:0.0}";
            }
            if (sol.AzimCorMil < 0)
            {
                this.textBox27.Text = "";
                this.textBox28.Text = "";
            }
            else
            {
                this.textBox27.Text = $@"{sol.AzimCorMil:0}";
                this.textBox28.Text = $@"{sol.AzimCorDeg:0.0}";
            }
            if (sol.AzimMil < 0)
            {
                this.textBox29.Text = "";
                this.textBox30.Text = "";
            }
            else
            {
                this.textBox29.Text = $@"{sol.AzimMil:0}";
                this.textBox30.Text = $@"{sol.AzimDeg:0.0}";
            }
            if (sol.ElevMil < 0)
            {
                this.textBox25.Text = "";
                this.textBox26.Text = "";
            }
            else
            {
                this.textBox25.Text = $@"{sol.ElevMil:0}";
                this.textBox26.Text = $@"{sol.ElevDeg:0.0}";
            }
            this.textBox31.Text = $@"{sol.Tof:0.0}";
        }
    }
}